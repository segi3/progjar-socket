import socket
import threading
from typing import Collection
import time

# friend list
from friendManager import FriendManager
fm = FriendManager()
print('Populating friends cache...', end='')
fm.load_friends()
print('done')

def read_msg(clients, sock_cli, addr_cli, username_cli):
    while True:
        #receive msg
        data = sock_cli.recv(65535)
        if len(data) == 0:
            break

        decoded = data.decode("utf-8").split("|")

        #cek jika tipe yang diterima file atau teks
        if len(decoded) > 2:
            #kirim file
            dest, filename, filesize = decoded
            dest_sock_cli = clients[dest][0]
            fileinfo = "{}|{}|{}".format(username_cli, filename, filesize)
            send_file(sock_cli, dest_sock_cli, fileinfo)

        else:

            #parsing msg
            dest, msg_raw = data.decode("utf-8").split("|")
            msg = "<{}>: {}".format(username_cli, msg_raw)

            print(msg)
            #relay msg to all client
            if dest == "bcast":
                send_broadcast(clients, msg, addr_cli)
            #friend list
            elif dest == "flist":
                dest_sock_cli = clients[username_cli][0]
                send_msg(dest_sock_cli, "{}".format(fm.generate_friend_list(username_cli)))
            #add friend
            elif dest == "fadd":
                dest_sock_cli = clients[username_cli][0]

                if fm.append_new_friends(username_cli, msg_raw):
                    send_msg(dest_sock_cli, "Berhasil menambahkan {} ke daftar pertemanan.".format(msg_raw))
                else:
                    send_msg(dest_sock_cli, "Gagal menambahkan {} ke daftar pertemanan.".format(msg_raw))

            #direct msg
            else:
                dest_sock_cli = clients[dest][0]
                send_msg(dest_sock_cli, msg)

    sock_cli.close()
    print("Connection Closed", addr_cli)

#send to all clients
def send_broadcast(clients, data, sender_addr_cli):
    for sock_cli, addr_cli, _ in clients.values():
        if not (sender_addr_cli[0] == addr_cli[0] and sender_addr_cli[1] == addr_cli[1]):
            send_msg(sock_cli, data)
#send direct msg
def send_msg(sock_cli, data):
    sock_cli.send(bytes(data, "utf-8"))
#send file
def send_file(sender_addr_cli, sock_cli, fileinfo):
    sock_cli.send(bytes(fileinfo, "utf-8"))
    time.sleep(0.1)

    total = 0
    sender, filename, filesize = fileinfo.split("|")
    print(filename, filesize)
    while True:
        if total >= int(filesize):
            break
        data = sender_addr_cli.recv(65535)
        # print("relaying " + str(data))
        # print("receiving " + len(data))
        try: 
            sock_cli.sendall(data)
            # time.sleep(0.1)
        except:
            sock_cli.close()

        total = total + len(data)


#create server socket object
sock_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#binding socket object to IP addr and certain port
sock_server.bind(("0.0.0.0", 6666))

#listen for an incoming connection
sock_server.listen(5)

#create dictionary for client's info
clients = {}

while True:
    #accept connection from clients
    sock_cli, addr_cli = sock_server.accept()

    #read client username
    username_cli = sock_cli.recv(65535).decode("utf-8")
    print(username_cli, "joined")

    #create new thread for msg read and relay the thread
    thread_cli = threading.Thread(target=read_msg, args=(clients, sock_cli, addr_cli, username_cli))
    thread_cli.start()

    #save client info to dictionary
    clients[username_cli] = (sock_cli, addr_cli, thread_cli)