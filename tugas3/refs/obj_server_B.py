import pickle
import socket
from obj_def_B import Person
from obj_def_B import Car

def print_object (obj):
    if isinstance(obj, Car):
        print("Brand: " + obj.brand)
        print("Series: " + obj.series)

    elif isinstance(obj, Person):
        print("Name: " + obj.name)
        print("Umur: " + str (obj.age))
    else:
        print ("Unknown Object")


sock_serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

sock_serv.bind(('0.0.0.0', 6666))

sock_serv.listen(5)

sock_cli, address = sock_serv.accept()

msg = sock_cli.recv(1024)

obj = pickle.loads(msg)
print_object(obj)
# print("Name: " + obj.name)
# print("Umur: " + str (obj.age))

msg = sock_cli.recv(1024)

obj = pickle.loads(msg)
print_object(obj)
# print("Brand: " + obj.brand)
# print("Series: " + obj.series)

sock_cli.close()
sock_serv.close()