import pickle
import socket
from obj_def_B import Person
from obj_def_B import Car

#make object socket
sock_cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#connect to server
sock_cli.connect(('127.0.0.1', 6666))

#make object to send
mylist = []

#mylist.append('This is a string')
#mylist.append(5)
#mylist.append(('localhost', 5000))

#print(mylist)

person = Person("Budi", 28)
print(person)

car = Car("Hyundai", "120")
print(car)

#serialization object
p_list = pickle.dumps(person)
print(p_list)

#send objecct
sock_cli.send(p_list)

p_list = pickle.dumps(car)
print(p_list)

#send objecct
sock_cli.send(p_list)

#close connection
sock_cli.close()