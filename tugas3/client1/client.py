import os
import socket
import sys
import threading
import time

def read_msg(sock_cli):
    while True:
        #receive msg
        data = sock_cli.recv(65535).decode("utf-8")
        if len(data) == 0:
            break

        if '|' in data:
            sender, filename, filesize = data.split("|")
            print(sender + filename + filesize)

            total = 0
            with open(filename, 'wb') as file:
                while True:
                    if total >= int(filesize):
                        break
                        file.close()
                    print("receiving")
                    data = sock_cli.recv(65535)
                    total = total + len(data)     
                    file.write(data)
            print("<" + str(sender) + "> " + filename + " received")

        else:
            print(data)

#create socket object
sock_cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#connect to server
sock_cli.connect(("127.0.0.1", 6666))

#send user info to server
sock_cli.send(bytes(sys.argv[1], "utf-8"))

#create thread for msg read
thread_cli = threading.Thread(target=read_msg, args=(sock_cli,))
thread_cli.start()

while True:
    # chose command
    user_command = input('command: ')

    #send/recv msg command
    if user_command == "pm":

        dest = input("masukkan username tujuan (ketikkan bcast untuk broadcast pesan):")
        msg = input("masukkan pesan anda:")
        sock_cli.send(bytes("{}|{}".format(dest, msg), "utf-8"))
    #broadcast command
    elif user_command == "bcast":

        dest = "bcast"
        msg = input("masukkan pesan anda:")
        sock_cli.send(bytes("{}|{}".format(dest, msg), "utf-8"))
    #friend list command
    elif user_command == "flist":
        sock_cli.send(bytes("{}|{}".format('flist', 'flist'), "utf-8"))
    #add friend command
    elif user_command == "fadd":
        msg = input("masukkan username:")
        sock_cli.send(bytes("{}|{}".format('fadd', msg), "utf-8"))
    #send file command
    elif user_command == "file":
        dest = input("masukkan username tujuan (ketikkan bcast untuk broadcast pesan):")
        filename = input("ketik nama file anda:")
        filesize = os.path.getsize(filename)
        sock_cli.send(bytes("{}|{}|{}".format(dest, filename, filesize), "utf-8"))
        time.sleep(0.5)

        with open(filename, "rb") as file:
            while True:
                data = file.read(65535)

                if not data:
                    break
                    file.close()
                
                print("sending")
                sock_cli.sendall(data)

            print("File sent successfully\n")

    #exit command
    elif user_command == "exit":
        sock_cli.close()
        break

    else:
        print('command tidak dapat dikenali, command:')
        