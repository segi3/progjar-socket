var1 = 'rafi'
var2 = 'rapui'
var3 = 'ediy'

friends_cache = []

# baca dari file
with open('friend_list.txt', 'r') as f:
    for pair in f.readlines():
        print(pair.strip('\n'))
        friends_cache.append(tuple(pair[:-1].split(',')))

# buat friend list
tmp = []
for fl in friends_cache:
    if fl[0] == var3 and fl[1] not in tmp:
        tmp.append(fl[1])
    elif fl[1] == var3 and fl[0] not in tmp:
        tmp.append(fl[0])

print(tmp)

# append ke cache
var4 = 'rohman'
var5 = 'ivan'
friends_cache.append((var4, var5))

# append langsung ke file
with open('friend_list.txt', 'a') as f:
    f.write('{0},{1}\n'.format(var4, var5))

# tulis ke file
with open('friend_list.txt', 'w') as f:
    for fl in friends_cache:
        f.write('{0},{1}\n'.format(fl[0], fl[1]))





