import socket
import select
import sys
from pathlib import Path
import os

import threading

# READ CONFIG FILE
res = {}
sections = []
with open('httpserver.conf', 'r') as f:
    for line in f:
        line = line.strip()
        if line.startswith('['):
            line = line.strip('[]')
            res[line] = {}
            sections.append(line)
        else:
            conf_keys = line.split('=')
            res[sections[-1]][conf_keys[0]] = conf_keys[1]


HOST = res['SERVER']['HOST']
PORT = int(res['SERVER']['PORT'])

server_address = (HOST, PORT)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(server_address)
server_socket.listen(5)

input_socket = [server_socket]

texts = ['txt']
images = ['png', 'jpeg', 'jpg']
videos = ['ogv', 'mp4']
audios = ['mp3', 'mpeg']

dir_path = os.path.dirname(os.path.realpath(__file__))

print('http server listening on port: 80..')

def handle(sock):
    while True:
        # receive data from client, break when null received          
        data = sock.recv(4096)
        
        data = data.decode('utf-8')

        response_header = b''
        response_data = b''

        request_header = data.split('\r\n')
        # print(request_header)


        if request_header[0] == '':
            continue

        
        request_file = request_header[0].split()[1]

        
        if request_file == 'index.html' or request_file == '/' or request_file == '/index.html':
            f = open('index.html', 'r')
            response_data = f.read()
            f.close()
            
            content_length = len(response_data)
            response_header = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length:' \
                                + str(content_length) + '\r\n\r\n'

            sock.sendall(response_header.encode('utf-8') + response_data.encode('utf-8'))

        elif request_file.startswith('/dataset/'):

            requested_file = request_file[1:].split('/')[1]

            requested_file = requested_file.replace('%20', ' ')

            file_type = requested_file.split('.')[1]

            if file_type in images:
                content_type = 'image/{}'.format(file_type)
            elif file_type in videos:
                content_type = 'video/{}'.format(file_type)
            elif file_type in texts:
                content_type == 'text/{}'.format(file_type)
            elif file_type in audios:
                content_type = 'audio/{}'.format(file_type)
            else:
                content_type = 'application/octet-stream'

            file_path = os.path.join(dir_path, 'dataset\\{}'.format(requested_file))
            
            if not os.path.isfile(file_path):
                with open('404.html', 'r') as f:
                    response_data = f.read()

                content_length = len(response_data)

                response_header = 'HTTP/1.1 404 Not Found\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length:' \
                                + str(content_length) + '\r\n\r\n'

                sock.sendall(response_header.encode('utf-8') + response_data.encode('utf-8'))
                # sock.sendall(b'HTTP/1.1 404 Not found\r\n\r\n')
                # print('file does not exists')
                continue

            with open(file_path, 'rb') as f:
                response_data = f.read()
            
            content_length = len(response_data)

            print('content-length: '+ str(content_length))

            # response_header = 'HTTP/1.1 200 OK\r\nContent-Type: image/png\r\nContent-Length:' + str(content_length) + '\r\n\r\n'

            # response_header = 'HTTP/1.1 200 OK\r\nContent-Dispotition: attachment; filename={}\r\nContent-Type: application/octet-stream\r\nContent-Transfer-Encoding: binary\r\nContent-Length:'.format(requested_file) + str(content_length) + '\r\n\r\n'
            # response_header = 'HTTP/1.1 200 OK\r\nContent-Type: application/octet-stream\r\nContent-Dispotition: attachment; filename={}\r\nContent-Transfer-Encoding: binary\r\nContent-Length:'.format(requested_file) + str(content_length) + '\r\n\r\n'

            response_header = 'HTTP/1.1 200 OK\r\nContent-Type: {}\r\nContent-Dispotition: attachment; filename={}\r\nContent-Transfer-Encoding: binary\r\nContent-Length:'.format('video/ogv', requested_file) + str(content_length) + '\r\n\r\n'


            sock.sendall(response_header.encode('utf-8') + response_data)

        else:
            with open('404.html', 'r') as f:
                response_data = f.read()

            content_length = len(response_data)

            response_header = 'HTTP/1.1 404 Not Found\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length:' \
                                + str(content_length) + '\r\n\r\n'

            sock.sendall(response_header.encode('utf-8') + response_data.encode('utf-8'))

# try:
while True:
    read_ready, write_ready, exception = select.select(input_socket, [], [])
    
    for sock in read_ready:
        if sock == server_socket:
            client_socket, client_address = server_socket.accept()
            input_socket.append(client_socket)                       

            thread = threading.Thread(target=handle, args=(client_socket,))
            thread.start()
            

# except KeyboardInterrupt:        
#     server_socket.close()
#     sys.exit(0)