import socket
import ssl
import re

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ssl_socket = ssl.wrap_socket(client_socket, ssl_version=ssl.PROTOCOL_TLSv1)

server_address_its = ('www.its.ac.id', 443)
server_address_classroom = ('classroom.its.ac.id', 443)

ssl_socket.connect(server_address_its)

request_header = b'HEAD / HTTP/1.1\r\nHost: www.its.ac.id\r\nAccept: */*\r\nAccept-Encoding: gzip, deflate, br\r\nConnection: keep-alive\r\n\r\n'
ssl_socket.send(request_header)

response = ''
while True:
    received = ssl_socket.recv(1024)
    response += received.decode('utf-8',errors='ignore')
    # print(received)
    if not received:
        break
    if re.search("Content-Encoding", received.decode("utf-8")) != "None":
        break

content = re.split('Content-Encoding: ', response)
print(content[1])
ssl_socket.close()