import socket
import ssl
import re
from bs4 import BeautifulSoup

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ssl_socket = ssl.wrap_socket(client_socket, ssl_version=ssl.PROTOCOL_TLSv1)

server_address_its = ('www.its.ac.id', 443)
server_address_classroom = ('classroom.its.ac.id', 443)

ssl_socket.connect(server_address_its)

request_header = b'GET / HTTP/1.0\r\nHost: classroom.its.ac.id\r\n\r\n'
ssl_socket.send(request_header)

response = ''
while True:
    received = ssl_socket.recv(1024)
    if not received:
        break
    response += received.decode('utf-8')

soup = BeautifulSoup(response, 'html.parser')
# print(soup)
nav = soup.find('ul', class_='navbar-nav h-100 wdm-custom-menus links').text

# navb = nav.encode('utf-8').replace(b' ', b'').replace(b'\n\n\n\n', b'')
# navhasil = navb.decode('utf-8')

print (nav)
ssl_socket.close()