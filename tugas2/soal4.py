import socket
import ssl
import re

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ssl_socket = ssl.wrap_socket(client_socket, ssl_version=ssl.PROTOCOL_TLSv1)

server_address_its = ('www.its.ac.id', 443)
server_address_classroom = ('classroom.its.ac.id', 443)

ssl_socket.connect(server_address_classroom)

request_header = b'HEAD / HTTP/1.0\r\nHost: classroom.its.ac.id\r\nConnection: close\r\nAccept-Encoding: gzip\r\n\r\n'
ssl_socket.send(request_header)

response = ''
while True:
    received = ssl_socket.recv(1024)
    if not received:
        break
    response += received.decode('utf-8')

spl_word ='Content-Type:'
x = re.split("\r\n",response)
x = re.split("charset=", x[3])
print(x[1])
ssl_socket.close()