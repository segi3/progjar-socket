import socket
import ssl
import re

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ssl_socket = ssl.wrap_socket(client_socket, ssl_version=ssl.PROTOCOL_TLSv1)

server_address_its = ('www.its.ac.id', 443)
server_address_classroom = ('classroom.its.ac.id', 443)

ssl_socket.connect(server_address_its)

request_header = b'HEAD / HTTP/1.0\r\nHost: www.its.ac.id\r\nConnection: close\r\nAccept-Encoding: gzip\r\n\r\n'
ssl_socket.send(request_header)

response = ''
while True:
    received = ssl_socket.recv(1024)
    if not received:
        break
    response += received.decode('utf-8')

spl_word ='Server:'
res = response.partition(spl_word)[0]
x = re.findall("[0-9][0-9][0-9]", res)
print(x[0], res.split(x[0], 1)[1])
ssl_socket.close()