class FriendManager:
    def __init__(self):
        self.friend_cache = []

    def load_friends(self):
        with open('friend_list.txt', 'r') as f: # baca file isi ke cache
            for pair in f.readlines():
                self.friend_cache.append(tuple(pair[:-1].split(',')))

    def generate_friend_list(self, username):
        tmp = []
        for fl in self.friend_cache:
            if fl[0] == username and fl[1] not in tmp:
                tmp.append(fl[1])
            elif fl[1] == username and fl[0] not in tmp:
                tmp.append(fl[0])

        response = 'daftar pertemanan:\n'

        if len(tmp) == 0:
            response+='\tbelum memiliki teman.'
            return response

        for fl in tmp:
            response+='\t- {}\n'.format(fl)
        
        return response
    
    def check_exist_username(self, username):

        for fl in self.friend_cache:
            if fl[0] == username or fl[1] == username:
                return True
        return False
    
    def append_new_friends(self, adder, friend):

        # check if both username is same
        if adder == friend:
            return False

        # check if exists in cache
        for fl in self.friend_cache:
            if fl[0] == adder:
                if fl[1] == friend:
                    print('duplicate entry')
                    return False
            elif fl[1] == adder:
                if fl[0] == friend:
                    print('duplicate entry')
                    return False

        # no same entry, append to cache
        self.friend_cache.append((adder, friend))

        with open('friend_list.txt', 'a') as f:
            f.write('{0},{1}\n'.format(adder, friend))
        
        return True