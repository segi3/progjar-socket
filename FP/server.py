import socket
import threading
from typing import Collection
import time

# friend list
from friendManager import FriendManager
fm = FriendManager()
print('Populating friends cache...', end='')
fm.load_friends()
print('done')

#active rooms
rooms = []

def read_msg(clients, sock_cli, addr_cli, username_cli):
    while True:
        #receive msg
        data = sock_cli.recv(65535)
        if len(data) == 0:
            break
        
        if '|' in data.decode('utf-8'):

            decoded = data.decode("utf-8").split("|")

            #cek jika tipe yang diterima file atau teks
            if len(decoded) > 2:
                #kirim file
                dest, filename, filesize = decoded
                dest_sock_cli = clients[dest][0]
                fileinfo = "{}|{}|{}".format(username_cli, filename, filesize)
                send_file(sock_cli, dest_sock_cli, fileinfo)

            else:

                #parsing msg
                dest, msg_raw = data.decode("utf-8").split("|")
                msg = "<{}>: {}".format(username_cli, msg_raw)


                #relay msg to all client
                if dest == "bcast":
                    send_broadcast(clients, msg, addr_cli)

                #friend list
                elif dest == "flist":
                    dest_sock_cli = clients[username_cli][0]
                    send_msg(dest_sock_cli, "{}".format(fm.generate_friend_list(username_cli)))

                #add friend
                elif dest == "fadd":
                    dest_sock_cli = clients[username_cli][0]

                    if fm.append_new_friends(username_cli, msg_raw):
                        send_msg(dest_sock_cli, "Berhasil menambahkan {} ke daftar pertemanan.".format(msg_raw))
                    else:
                        send_msg(dest_sock_cli, "Gagal menambahkan {} ke daftar pertemanan.".format(msg_raw))

                #direct msg
                else:
                    dest_sock_cli = clients[dest][0]
                    send_msg(dest_sock_cli, msg)

        elif ';' in data.decode('utf-8'): # game

            #parsing msg
            game_ar = data.decode("utf-8").split(";")

            if game_ar[1] == "class_select": # game;class_select;{nomor character}
                dest_sock_cli = clients[username_cli][0]
                send_msg(dest_sock_cli, "game;class_select;{}".format(game_ar[2])) # game;change_turn;{nama yg ngirim};{nama lawannya}

            # * game: challenge
            elif game_ar[1] == "challenge": # game;challenge;{enemy};{challenger class num}

                #! perlu di cek udah ada di dalem room lain atau belum
                in_match = 0
                for room in rooms:
                    for player in room:
                        if player == username_cli:
                            # print('room {} - {}'.format(room, player))
                            in_match = 1
                            break
                        elif player == game_ar[2]:
                            in_match = -1
                            break

                if in_match == 1:
                    challenge_msg = "{} sedang berada di game room lain".format("anda")
                    send_msg(sock_cli, challenge_msg)
                elif in_match == -1:
                    challenge_msg = "{} sedang berada di game room lain".format(game_ar[2])
                    send_msg(sock_cli, challenge_msg)
                else :
                    rooms.append((username_cli, game_ar[2]))
                    # kirim challenge ke lawan
                    dest_sock_cli = clients[game_ar[2]][0]
                    challenge_msg = "game;challenge;{};{}".format(username_cli, game_ar[3])
                    send_msg(dest_sock_cli, challenge_msg)
                print(rooms)
                #! kalau di tolak harus di hapus dari list

            
            elif game_ar[1] == "respond_ch": # game;respond_ch;{y/n};{challenger};{class num}
                
                if (game_ar[2] == 'y' or game_ar[2] == 'yes'):
                    msg = 'game;respond_ch;{};{};{}'.format('y', username_cli, game_ar[4])
                else:
                    msg = 'game;respond_ch;{};{}'.format('n', username_cli)
                    delete_room(username_cli)

                dest_sock_cli = clients[game_ar[3]][0]
                send_msg(dest_sock_cli, msg)
            
            elif game_ar[1] == "change_turn": # game;change_turn;{nama yg sekarang punya giliran}

                print("[{}'s turn]".format(game_ar[2]))

                dest_sock_cli = clients[game_ar[2]][0]
                send_msg(dest_sock_cli, "game;change_turn;{};{}".format(username_cli, game_ar[2])) # game;change_turn;{nama yg ngirim};{nama lawannya}
            
            elif game_ar[1] == "change_turn2": # game;change_turn;{nama yg sekarang punya giliran}
                #! ^fungsi sementara, change turn yg gak pake nama lawan, harus di cari sendiri di server nama lawannya
                for players in rooms:
                    if players[0] == username_cli:
                        changeTo = players[1]
                    elif players[1] == username_cli:
                        changeTo = players[0]

                print("[{}'s turn]".format(changeTo))

                dest_sock_cli = clients[changeTo][0]
                send_msg(dest_sock_cli, "game;change_turn;{};{}".format(username_cli, changeTo)) # game;change_turn;{nama yg ngirim};{nama lawannya}

            elif game_ar[1] == "self_end_turn": #game:self_end_turn

                print("[{} ends turn]".format(username_cli))

                dest_sock_cli = clients[username_cli][0]
                send_msg(dest_sock_cli, "game;self_end_turn") # game;change_turn;{nama yg ngirim};{nama lawannya}

            elif game_ar[1] == "attack": # game;attack;{attack damage}

                # check if is in a game, and get enemy's name
                for players in rooms:
                    if players[0] == username_cli:
                        attackTo = players[1]
                    elif players[1] == username_cli:
                        attackTo = players[0]
                    else:
                        dest_sock_cli = clients[username_cli][0]
                        send_msg(dest_sock_cli, "game;not_in_game_error") # game;not_in_game_error
                
                dest_sock_cli = clients[attackTo][0]
                send_msg(dest_sock_cli, "game;attacked;{};{}".format(game_ar[2], username_cli)) # game;attacked;{attack damage};{attacker}

                dest_sock_cli = clients[username_cli][0]
                send_msg(dest_sock_cli, "game;attack;{}".format(game_ar[2])) # game;attacked;{attack damage}

            elif game_ar[1] == "heal": # gane;heal;{ heal damage }
                
                # check if is in a game, and get enemy's name
                for players in rooms:
                    if players[0] == username_cli:
                        enemy = players[1]
                    elif players[1] == username_cli:
                        enemy = players[0]
                    else:
                        dest_sock_cli = clients[username_cli][0]
                        send_msg(dest_sock_cli, "game;not_in_game_error") # game;not_in_game_error

                dest_sock_cli = clients[username_cli][0]
                send_msg(dest_sock_cli, "game;heal;{}".format(game_ar[2])) # game;heal;{heal damage}
                print('',end='')

                dest_sock_cli = clients[enemy][0]
                send_msg(dest_sock_cli, "game;heal2;{};{}".format(game_ar[2], enemy)) # game;heal2;{heal damage};{attacker}
                print('=====send heal to enemy stat')

            elif game_ar[1] == "util_mp_enemy_reduce": #game;util_mp_enemy_reduce;{cost}

                # check if is in a game, and get enemy's name
                for players in rooms:
                    if players[0] == username_cli:
                        enemy = players[1]
                    elif players[1] == username_cli:
                        enemy = players[0]
                    else:
                        dest_sock_cli = clients[username_cli][0]
                        send_msg(dest_sock_cli, "game;not_in_game_error") # game;not_in_game_error

                dest_sock_cli = clients[enemy][0]
                send_msg(dest_sock_cli, "game;util_mp_enemy_reduce;{};{};{}".format(game_ar[2], enemy, username_cli)) # game;heal2;{heal damage};{attacker}

            elif game_ar[1] == "end": #game;end;{winner}
                print('game ended')
                room_count = 0
                delete_room(username_cli)
                    


    sock_cli.close()
    print("Connection Closed", addr_cli)

def delete_room(username_cli):
    room_count = 0
    for room in rooms:
        for player in room:
            if player == username_cli:
                # print('room {} - {}'.format(room, player))
                del rooms[room_count]
        room_count += 1

#send to all clients
def send_broadcast(clients, data, sender_addr_cli):
    for sock_cli, addr_cli, _ in clients.values():
        # if not (sender_addr_cli[0] == addr_cli[0] and sender_addr_cli[1] == addr_cli[1]):
        send_msg(sock_cli, data)


#send direct msg
def send_msg(sock_cli, data):
    sock_cli.send(bytes(data, "utf-8"))


#send file
def send_file(sender_addr_cli, sock_cli, fileinfo):
    sock_cli.send(bytes(fileinfo, "utf-8"))
    time.sleep(0.1)

    total = 0
    sender, filename, filesize = fileinfo.split("|")
    print(filename, filesize)
    while True:
        if total >= int(filesize):
            break
        data = sender_addr_cli.recv(65535)
        # print("relaying " + str(data))
        # print("receiving " + len(data))
        try: 
            sock_cli.sendall(data)
            # time.sleep(0.1)
        except:
            sock_cli.close()

        total = total + len(data)


#create server socket object
sock_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#binding socket object to IP addr and certain port
sock_server.bind(("0.0.0.0", 6666))

#listen for an incoming connection
sock_server.listen(5)

#create dictionary for client's info
clients = {}

while True:
    #accept connection from clients
    sock_cli, addr_cli = sock_server.accept()

    #read client username
    username_cli = sock_cli.recv(65535).decode("utf-8")
    print(username_cli, "joined")

    #create new thread for msg read and relay the thread
    thread_cli = threading.Thread(target=read_msg, args=(clients, sock_cli, addr_cli, username_cli))
    thread_cli.start()

    #save client info to dictionary
    clients[username_cli] = (sock_cli, addr_cli, thread_cli)