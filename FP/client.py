import os
import socket
import sys
import threading
import time

from rpg.character import Character
from rpg.magic import Spell
from rpg.item import Item

def read_msg(pclass_arg, sock_cli):

    in_game = False
    room = 'main'
    challenger = ''
    turn = False

    pnum = ''
    pclass = ''
    emclass = ''

    HPmax = 100
    HPcur = 100

    while True:
        #receive msg
        data = sock_cli.recv(65535).decode("utf-8")
        if len(data) == 0:
            break

        if '|' in data:
            sender, filename, filesize = data.split("|")
            print(sender + filename + filesize)

            total = 0
            with open(filename, 'wb') as file:
                while True:
                    if total >= int(filesize):
                        break
                        file.close()
                    print("receiving")
                    data = sock_cli.recv(65535)
                    total = total + len(data)     
                    file.write(data)
            print("<" + str(sender) + "> " + filename + " received")

        # untuk game
        elif ';' in data:
            game_ar = data.split(";")

            if game_ar[1] == "class_select": # game;class_select;{nomor character}
                
                pclass = pclass_arg
                pnum = game_ar[2]

            if game_ar[1] == "challenge": # game;challenge;{challenger};{challenger class num}

                respond_ch = input("<" + str(game_ar[2]) + "> has challenged you! Would you like to accept the challenge? y/n: ")

                if respond_ch.lower() == 'y'or respond_ch.lower() == 'yes':
                    room = str(game_ar[2])
                    challenger = str(game_ar[2])
                    in_game = True

                    if game_ar[3] == '1':
                        spells = [
                            Spell('Heavy Slash', 40, 100, 'black'),
                            Spell('Lacerate', 70, 200, 'black'),
                            Spell('Purify', 20, 50, 'white')
                        ]
                        items = [
                            {"item": Item("Sandvich", "potion", "Heals full HP", 1000), "quantity": 1},
                            {"item": Item("Boiling water", "attack", "Burn your enemy's skin", 70), "quantity": 2}
                        ]
                        emclass = Character('Warrior', 500, 70, 70, 200, spells, items)

                    elif game_ar[3] == '2':
                        spells = [
                            Spell('Lightning Zap', 30, 70, 'black'),
                            Spell('Thunderstrike', 90, 200, 'black'),
                            Spell('Cold Embrace', 50, 100, 'white')
                        ]
                        items = [
                            {"item": Item("Clarity", "potion", "Restores 40 MP", 40), "quantity": 2},
                            {"item": Item("Faerie Fire", "potion", "Restores 50 HP", 50), "quantity": 2}
                        ]
                        emclass = Character('Mage', 250, 150, 20, 40, spells, items)

                    elif game_ar[3] == '3':
                        spells = [
                            Spell('Blink Strike', 40, 70, 'black'),
                            Spell('Dark Pact', 110, 200, 'black'),
                            Spell("Shadow's attendant", 20, 50, 'white')
                        ]
                        items = [
                            {"item": Item("Clarity", "potion", "Restores 40 MP", 40), "quantity": 2},
                            {"item": Item("Poison", "attack", "Poison your enemy", 90), "quantity": 2}
                        ]
                        emclass = Character('Rogue', 300, 120, 40, 100, spells, items)


                sock_cli.send(bytes("game;respond_ch;{};{};{}".format(respond_ch.lower(), str(game_ar[2]), str(pnum)), "utf-8"))

            elif game_ar[1] == "respond_ch": # game;respond_ch;{y/n};{enemy yg di challenge};{enemy class num}

                if (game_ar[2] == 'y' or game_ar[2] == 'yes'):
                    print('{} accepted your challenge'.format(game_ar[3]))
                    room = str(game_ar[3])
                    challenger = str(game_ar[3])
                    in_game = True

                    if game_ar[4] == '1':
                        spells = [
                            Spell('Heavy Slash', 40, 100, 'black'),
                            Spell('Lacerate', 70, 200, 'black'),
                            Spell('Purify', 20, 50, 'white')
                        ]
                        items = [
                            {"item": Item("Sandvich", "potion", "Heals full HP", 1000), "quantity": 1},
                            {"item": Item("Boiling water", "attack", "Burn your enemy's skin", 70), "quantity": 2}
                        ]
                        emclass = Character('Warrior', 500, 70, 70, 200, spells, items)

                    elif game_ar[4] == '2':
                        spells = [
                            Spell('Lightning Zap', 30, 70, 'black'),
                            Spell('Thunderstrike', 90, 200, 'black'),
                            Spell('Cold Embrace', 50, 100, 'white')
                        ]
                        items = [
                            {"item": Item("Clarity", "potion", "Restores 40 MP", 40), "quantity": 2},
                            {"item": Item("Faerie Fire", "potion", "Restores 50 HP", 50), "quantity": 2}
                        ]
                        emclass = Character('Mage', 250, 150, 20, 40, spells, items)

                    elif game_ar[4] == '3':
                        spells = [
                            Spell('Blink Strike', 40, 70, 'black'),
                            Spell('Dark Pact', 110, 200, 'black'),
                            Spell("Shadow's attendant", 20, 50, 'white')
                        ]
                        items = [
                            {"item": Item("Clarity", "potion", "Restores 40 MP", 40), "quantity": 2},
                            {"item": Item("Poison", "attack", "Poison your enemy", 90), "quantity": 2}
                        ]
                        emclass = Character('Rogue', 300, 120, 40, 100, spells, items)

                    # turn pertama untuk yg di challenge
                    sock_cli.send(bytes("game;change_turn;{}".format(str(game_ar[3])), "utf-8")) #game;change_turn;{enemy yg di challenge}

                else:
                    print('{} declined your challenge'.format(game_ar[3]))
                    
                    #! hapus room di server

            elif game_ar[1] == "change_turn":

                if in_game:
                    print('\nit is now your turn! choose:attack/spell')

                    # change turn
                    turn = True

            elif game_ar[1] == "self_end_turn":
                print('\nyou ended your turn!')

                turn = False
            
            elif game_ar[1] == "attack": # game;attack;{} # fungsi untuk ngurangin darah lawan di state sendiri

                emclass.take_damage(int(game_ar[2]))

                print('you attacked for {} damage'.format(game_ar[2]))

                pclass.get_stats()
                emclass.get_stats()

                if emclass.get_hp() <= 0:
                    in_game = False
                    room = 'main'
                    challenger = ''
                    turn = False

                    pclass.reset()
                    emclass = ''

                    print("\nYOU WIN!!")
                    sock_cli.send(bytes("game;end", "utf-8"))
            
            elif game_ar[1] == "attacked": # game;attacked;{attack damage};{attacker}
                print('you are attacked with {} damage'.format(game_ar[2]))
                pclass.take_damage(int(game_ar[2]))

                pclass.get_stats()
                emclass.get_stats()

                if pclass.get_hp() <= 0:
                    in_game = False
                    room = 'main'
                    challenger = ''
                    turn = False

                    pclass.reset()
                    emclass = ''

                    print("\nYOU LOST TO {} :(".format(game_ar[3]))
                    sock_cli.send(bytes("game;end", "utf-8"))

            elif game_ar[1] == "heal": # game;heal;{heal damage}

                pclass.heal(int(game_ar[2]))

                print('you healed for {} HP'.format(game_ar[2]))

                pclass.get_stats()
                emclass.get_stats()
            
            elif game_ar[1] == "heal2": # game;heal2;{heal damage};{attacker}

                emclass.heal(int(game_ar[2]))

                print('enemy healed for {} HP'.format(game_ar[2]))

                pclass.get_stats()
                emclass.get_stats()
            
            elif game_ar[1] == "util_mp_enemy_reduce": # game;util_mp_enemy_reduce;{spell cost};{yg mp nya dikurang};{lawannya}

                emclass.reduce_mp(int(game_ar[2]))
                
            elif game_ar[1] == "end": # game;end;{winner}
                print("YOU WIN!!!!!!")
                continue

            elif game_ar[1] == "not_in_game_error":
                print('you are currently not in a game!!')

        else:
            print(data)

char_class = input('choose your character class!\n1-Warrior\n2-Mage\n3-Rogue\nyour selection (1/2/3):')

if str(char_class) == '1':
    
    spells = [
        Spell('Heavy Slash', 40, 100, 'black'),
        Spell('Lacerate', 70, 200, 'black'),
        Spell('Purify', 20, 50, 'white')
    ]
    items = [
        {"item": Item("Sandvich", "potion", "Heals full HP", 1000), "quantity": 1},
        {"item": Item("Boiling water", "attack", "Burn your enemy's skin", 70), "quantity": 2}
    ]
    player_class = Character('Warrior', 500, 70, 70, 200, spells, items)

elif str(char_class) == '2':
    
    spells = [
        Spell('Lightning Zap', 30, 70, 'black'),
        Spell('Thunderstrike', 90, 200, 'black'),
        Spell('Cold Embrace', 50, 100, 'white')
    ]
    items = [
        {"item": Item("Clarity", "potion", "Restores 40 MP", 40), "quantity": 2},
        {"item": Item("Faerie Fire", "potion", "Restores 50 HP", 50), "quantity": 2}
    ]
    player_class = Character('Mage', 250, 150, 20, 40, spells, items)

elif str(char_class) == '3':
    
    spells = [
        Spell('Blink Strike', 40, 70, 'black'),
        Spell('Dark Pact', 110, 200, 'black'),
        Spell("Shadow's attendant", 20, 50, 'white')
    ]
    items = [
        {"item": Item("Clarity", "potion", "Restores 40 MP", 40), "quantity": 2},
        {"item": Item("Poison", "attack", "Poison your enemy", 90), "quantity": 2}
    ]
    player_class = Character('Rogue', 300, 120, 40, 100, spells, items)

#create socket object
sock_cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#connect to server
sock_cli.connect(("127.0.0.1", 6666))

#send user info to server
sock_cli.send(bytes(sys.argv[1], "utf-8"))

#create thread for msg read
thread_cli = threading.Thread(target=read_msg, args=(player_class, sock_cli,))
thread_cli.start()

sock_cli.send(bytes("game;{};{}".format('class_select', char_class), "utf-8"))

while True:
    # chose command
    user_command = input('main command: \n')

    #game: challenge
    if user_command == "challenge":
        dest = "challenge"
        enemy = input("masukkan username tujuan:")
        sock_cli.send(bytes("game;{};{};{}".format(dest, enemy, char_class), "utf-8"))
    
    elif user_command == "attack":

        #! kasih cek apa lagi ingame atau engga
        #! kasih cek juga untuk turn

        att = player_class.generate_damage()

        sock_cli.send(bytes("game;{}".format('self_end_turn'), "utf-8"))
        sock_cli.send(bytes("game;{};{}".format("attack", att), "utf-8"))
        time.sleep(1)
        sock_cli.send(bytes("game;{}".format('change_turn2'), "utf-8")) #! sementara beda fungsi (tanpa param nama lawan)
    
    elif user_command == "spell":

        #! kasih cek apa lagi ingame atau engga
        #! kasih cek juga untuk turn

        player_class.choose_magic()
        spelnum = int(input('choose spell: ')) - 1

        spell = player_class.magic[spelnum]

        if spell.get_cost() > player_class.get_mp():
            print('not enough mp')
            continue

        damage = spell.generate_damage()
        cost = spell.get_cost()
        player_class.reduce_mp(cost)

        sock_cli.send(bytes("game;{};{}".format('util_mp_enemy_reduce', cost), "utf-8"))
        print('reduce your mp by {}'.format(cost))

        sock_cli.send(bytes("game;{}".format('self_end_turn'), "utf-8"))
        # print('=====turn end')

        if spell.get_type() == "white":

            sock_cli.send(bytes("game;{};{}".format('heal', damage), "utf-8"))
            print('you cast white magic to your self!')

        elif spell.get_type() == "black":

            sock_cli.send(bytes("game;{};{}".format('attack', damage), "utf-8"))
            print('you cast black magic to your enemy!')

        time.sleep(1)
        sock_cli.send(bytes("game;{}".format('change_turn2'), "utf-8")) #! sementara beda fungsi (tanpa param nama lawan)

    #direct message
    elif user_command == "pm":
        dest = input("masukkan username tujuan (ketikkan bcast untuk broadcast pesan):")
        msg = input("masukkan pesan anda:")
        sock_cli.send(bytes("{}|{}".format(dest, msg), "utf-8"))

    #broadcast command
    elif user_command == "bcast":
        dest = "bcast"
        msg = input("masukkan pesan anda:")
        sock_cli.send(bytes("{}|{}".format(dest, msg), "utf-8"))

    #friend list command
    elif user_command == "flist":
        sock_cli.send(bytes("{}|{}".format('flist', 'flist'), "utf-8"))

    #add friend command
    elif user_command == "fadd":
        msg = input("masukkan username:")
        sock_cli.send(bytes("{}|{}".format('fadd', msg), "utf-8"))

    #send file command
    elif user_command == "file":
        dest = input("masukkan username tujuan (ketikkan bcast untuk broadcast pesan):")
        filename = input("ketik nama file anda:")
        filesize = os.path.getsize(filename)
        sock_cli.send(bytes("{}|{}|{}".format(dest, filename, filesize), "utf-8"))
        time.sleep(0.5)

        with open(filename, "rb") as file:
            while True:
                data = file.read(65535)

                if not data:
                    break
                    file.close()
                
                print("sending")
                sock_cli.sendall(data)

            print("File sent successfully\n")

    #exit command
    elif user_command == "exit":
        sock_cli.close()
        break

    else:
        print('command tidak dapat dikenali, command:')