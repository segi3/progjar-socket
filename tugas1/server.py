import socket
import select
# import Queue
import sys
import os

from dotenv import load_dotenv
load_dotenv()

HOST = os.environ.get("HOST", "192.168.1.8")
PORT = int(os.environ.get("PORT", "27015"))

BUFFER_SIZE = 4096
SEPARATOR = '<:>'

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setblocking(0)

server.bind((HOST, PORT))
server.listen(3)
print('Server is listening..')

# select
# socket expected to read
inputs = [server]

# sockets to write
outputs = []

# outgoing message queeus
message_queues = {}

client_buffer = []
client_files = []

def set_list(l, i, v, b):
    if not b:
        try:
            l[i] = v
        except IndexError:
            for _ in range(i-len(l)+1):
                l.append(None)
            l[i] = v
    elif b:
        try:
            if l[i] == None:
                raise IndexError;
            l[i] += v
        except IndexError:
            for _ in range(i-len(l)+1):
                l.append(None)
            l[i] = bytearray()
            l[i] += v

def write_file(fn, d):
    # receiving files and writing as binary
    print('writing file {}..'.format(fn))
    
    with open(fn, 'wb') as f:
        f.write(d)

    print('Isi file sudah diterima')

try:
    while inputs:

        r, w, e = select.select(inputs, outputs, inputs)

        for s in r:
            if s == server: # s adalah server yang menerima koneksi
                client, address = server.accept()
                print('connected to {}'.format(address))
                inputs.append(client)
                # client.setblocking(0)
            
            else:
                # receiving header
                data = s.recv(BUFFER_SIZE)
                idx = inputs.index(s)

                if data[0:6] == b'<HEAD>':
                    ack, filename, filesize = data.decode('utf-8').split(SEPARATOR)
                    filename = os.path.basename(filename)
                    set_list(client_files, idx, filename, False) # simpen nama file
                    # print(client_files)

                    # print('receiving..\nfilename: {}\nfilesize: {}'.format(filename, filesize))
                    print('unggah {} dengan besar {} byte(s)'.format(filename, filesize))
                
                elif data == b'<END>':

                    # print(len(client_buffer[idx]))
                    
                    write_file(client_files[idx], client_buffer[idx])

                    s.send('<OK>'.encode('utf-8')) #informing client file is sent
                
                # elif data == b'':
                    # print('Data kosong')
                else:
                    if data[-5:] == b'<END>': # case flag END ke concat di paket terakhir stream file
                        set_list(client_buffer, idx, data, True)

                        write_file(client_files[idx], client_buffer[idx])
                    
                        s.send('<OK>'.encode('utf-8')) #informing client file is sent

                        # print(len(client_buffer[idx]))

                    set_list(client_buffer, idx, data, True)


                # client.close()
                # server.close()

except KeyboardInterrupt:
    server.close()
    sys.exit(0)

except Exception as e:
    print(e)
    server.close()
    sys.exit(0)

print(inputs)
print('selesai')
