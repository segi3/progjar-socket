import socket
import os
import sys
import threading
import select

from dotenv import load_dotenv
load_dotenv()

HOST = os.environ.get("HOST", "192.168.1.8")
PORT = int(os.environ.get("PORT", "27015"))

BUFFER_SIZE = 4096
SEPARATOR = '<:>'

# nama file
filename = os.environ.get("TEST_FILE", "testing.pdf")

# size file
filesize = os.path.getsize(filename)

# buat socket
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('Connecting to {}:{}..'.format(HOST, PORT))

server.connect((HOST, PORT))
print('Connected.')

try:
    while True:
        # sending header: filename and filesize
        # print('{}{}{}{}{}'.format('<HEAD>', SEPARATOR, filename, SEPARATOR, filesize))
        print('Mengunggah {}'.format(filename))
        server.send('{}{}{}{}{}'.format('<HEAD>', SEPARATOR, filename, SEPARATOR, filesize).encode('utf-8'))

        # reading file in binary and sending it
        n = 0
        with open(filename, 'rb') as f:
            while True:
                f_bytes = f.read(BUFFER_SIZE)
                # print(f_bytes)
                if not f_bytes:
                    server.send('<END>'.encode('utf-8'))
                    break
                server.sendall(f_bytes)
                # print(len(f_bytes))

        while True:
            msg = server.recv(BUFFER_SIZE)

            if msg == b'<OK>':
                print('Isi file sudah diterima')
                # break
        # break

except KeyboardInterrupt:
    server.close()
    sys.exit(0)

except Exception as e:
    print(e)
    server.close()
    sys.exit(0)